import { Component, OnInit } from '@angular/core';
import { DialogService, DialogComponent } from 'ng2-bootstrap-modal';
import { UserService } from 'src/app/services/user.service';
import { RequestsService } from 'src/app/services/requests.service';
import { User } from 'src/app/interfaces/user';
// import { AngularFireDatabase } from '@angular/fire/database';

export interface PromptModel {
  scope: any;
  currentReq: any;
}

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent extends DialogComponent<PromptModel, any> implements PromptModel {

  scope: any;
  currentReq: any;
  nombre: string;

  friends: User[];

  // tslint:disable-next-line:no-inferrable-types
  shouldAdd: string = 'yes';

  constructor(public _ds: DialogService,
              private _us: UserService,
              public _rs: RequestsService) {
                super(_ds);

     this._us.getUsers().valueChanges()
     .subscribe(( data: User[] ) => {
       this.friends = data;
       // almacena en un array los friends y luego compara el uid para mostrar el nombre
      for (let i = 0; i < this.friends.length; i++) {
        if (this.currentReq.remitente === this.friends[i].uid) {
         return this.nombre = this.friends[i].nick;
        }
      }
     }, (error) => {
       console.log(error);
     });
  }

  accept() {
    if ( this.shouldAdd === 'yes' ) {
      this._rs.setReqStatus(this.currentReq, 'accepted').then((data) => {
        console.log(data);
        this._us.addFriend(this.scope.user.uid, this.currentReq.remitente).then(() => {
          alert('solicitud aceptada');
        });
      }).catch((error) => {
        console.log(error);
      });

    } else if ( this.shouldAdd === 'no') {
      this._rs.setReqStatus(this.currentReq, 'rejected').then((data) => {
        console.log(data);
      }).catch((error) => {
        console.log(error);
      });

    } else if ( this.shouldAdd === 'later') {
      this._rs.setReqStatus(this.currentReq, 'later').then((data) => {
        console.log(data);
      }).catch((error) => {
        console.log(error);
      });
    }
  }



}
