import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { RequestsService } from './services/requests.service';
import { User } from 'firebase';
import { DialogService } from 'ng2-bootstrap-modal';
import { RequestComponent } from './modals/request/request.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'txt.me';
  user: User;
  requests: any[] = [];
  mailShown: any[] = [];

  constructor(private router: Router,
              private _as: AuthenticationService,
              private _us: UserService,
              private _rs: RequestsService,
              private _ds: DialogService) {

    this._as.getStatus().subscribe((status) => {
      this._us.getUserId(status.uid).valueChanges().subscribe((data: User) => {
        this.user = data;
        this._rs.getReqForEmail(this.user.email).valueChanges().subscribe((request: any) => {
          this.requests = request;
          this.requests = this.requests.filter((r) => {
            return r.status !== 'accepted' && r.status !== 'rejected';

          });

          this.requests.forEach((r) => {
            if (this.mailShown.indexOf(r.remitente ) === -1) {
              this.mailShown.push((r.remitente));
              this._ds.addDialog(RequestComponent, {scope: this, currentReq: r});
            }
          });

        }, (error) => {
          console.log(error);
        });
      });
    });
              }

}
