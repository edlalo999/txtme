import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  @Input() uid: any;
  // tslint:disable-next-line:no-inferrable-types
  // tslint:disable-next-line:quotemark
  last = "#last";
  contact: User;

  constructor(private _us: UserService) { }

  ngOnInit() {
    this._us.getUserId(this.uid).valueChanges()
            .subscribe((data: User) => {
              this.contact = data;
    });
  }

}
