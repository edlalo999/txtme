import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../interfaces/user';
import { UserService } from '../services/user.service';
import { ConversationService } from '../services/conversation.service';
import { AuthenticationService } from '../services/authentication.service';
import { AngularFireStorage } from '@angular/fire/storage';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-chat-new',
  templateUrl: './chat-new.component.html',
  styleUrls: ['./chat-new.component.css']
})
export class ChatNewComponent implements OnInit {


  friendId: any;
  friend: User;
  user: User;
  chat_id: string;
  textMessage: string;
  conversation: any;
  shake = false;

  imageChangedEvent: any = '';
  croppedImage: any = '';
  picture: any;

  bandera: boolean;


  constructor(private route: ActivatedRoute,
              private _us: UserService,
              private _cs: ConversationService,
              private _as: AuthenticationService,
              private _fs: AngularFireStorage
               ) {
    this.friendId = this.route.snapshot.params['uid'];

    // console.log(this.friendId);

    this._as.getStatus().subscribe((session) => {
      this._us.getUserId(session.uid).valueChanges()
              .subscribe((user: User) => {
                this.user = user;
                console.log(this.user);
                // devuelve el uid del usuario en sesión
                this._us.getUserId(this.friendId).valueChanges()
                        .subscribe(( data: User) => {
                          this.friend = data;
                          console.log(this.friend.avatar);
                          const chat = [
                            this.user.uid,
                            this.friend.uid
                          ].sort();
                          this.chat_id = chat.join('|');
                          // console.log(data);
                          this.show_Chat();

                        }, (error) => {
                          console.log(error);
                        });
              });
    });
   }

    ngOnInit() {
    //   jQuery('#CH').animate({
    //     scrollTop: 290
    // }, 2000);


        const posicion = $('#last').offset().top;
        jQuery('#CH').animate({
            scrollTop: posicion
        }, 1000);
    }



  sendFoto(data) {
    if (this.textMessage == null) {
      this.textMessage = this.user.nick + ' ha enviado una imagen';
    }
    const message = {
      uid: this.chat_id,
      timestamp: Date.now(),
      text: this.textMessage,
      remitente: this.user.uid,
      destinatario: this.friend.uid,
      img: data,
      type: 'foto'
    };
    this._cs.createChat(message).then(() => {
      this.textMessage = '';
    });
  }

  sendMessage() {
    const message = {
      uid: this.chat_id,
      timestamp: Date.now(),
      text: this.textMessage,
      remitente: this.user.uid,
      destinatario: this.friend.uid,
      type: 'text'
    };
    this._cs.createChat(message).then(() => {
      this.textMessage = '';
    });
    this.bandera = false;
  }

  sendZumbido() {
    const message = {
      uid: this.chat_id,
      timestamp: Date.now(),
      text: this.user.nick + ' Ha enviado un zumbido!',
      remitente: this.user.uid,
      destinatario: this.friend.uid,
      type: 'zumbido'
    };
    this._cs.createChat(message).then(() => {});
    this.doZumbido();
  }

  doZumbido() {
    const audio = new Audio('../../assets/sound/zumbido.m4a');
    audio.play();
    this.shake = true;
    window.setTimeout(() => {
      this.shake = false;
    }, 1000);
  }

  show_Chat( ) {
    this._cs.getChat(this.chat_id).valueChanges()
        .subscribe((data) => {
          // console.log(data);
          this.conversation = data;
              this.conversation.forEach((m) => {
                // se pasa como parametro el mensaje 'm'
                if (!m.seen) {
                  m.seen = true;
                  this._cs.editChat(m);

                  if (m.type === 'zumbido') {
                    this.doZumbido();

                  } else if (m.type !== 'zumbido') {
                    const new_m = new Audio('../../assets/sound/new_message.m4a');
                    new_m.play();
                  }
                }
              });
          // console.log(this.conversation.length);
        }, (error) => {
          console.log(error);
        });
  }


  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.bandera = true;
  }
  imageCropped(event: any) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  loadImageFailed() {
      // show message
  }


  saveFoto() {
    if (this.croppedImage) {
      const currentimageid = Date.now();
      const pictures = this._fs.ref( 'chats/' + currentimageid + '.png' )
          .putString( this.croppedImage, 'data_url' );
          // con el putString lo convierte en imagen binaria
          pictures.then((resp) => {
              this.picture = this._fs.ref( 'chats/' + currentimageid + '.png' )
              .getDownloadURL();
              // devuelve un objeto, por eso nos subscribimos en la siguiente linea
              this.picture.subscribe((p) => {
                // console.log(p);
                this.sendFoto(p);
                alert('todo ok');
              });
            }).catch((error) => {
              console.log(error);
          });
    } else {
      alert('error');
    }
    this.bandera = false;
  }
}
