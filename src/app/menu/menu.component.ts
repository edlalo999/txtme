import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor( private _as: AuthenticationService) { }

  logout() {
    this._as.logOut().then( () => {
      alert('sesión cerrada');
    })
      .catch(( error ) => {
        alert( 'Ocurrió un error' + error);
        console.log(error);
    });
  }

  ngOnInit() {
  }

}
