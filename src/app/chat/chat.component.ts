import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from 'firebase';
import { AuthenticationService } from '../services/authentication.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RequestsService } from '../services/requests.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  user: User;
  amigos: User[];
  query: string;

  // tslint:disable-next-line:no-inferrable-types
  friendEmail: string = '';
  closeResult: string;

  constructor(private _us: UserService,
              private _as: AuthenticationService,
              private modalService: NgbModal,
              private _rs: RequestsService) {

    this._us.getUsers().valueChanges()
                        .subscribe(( data: User[] ) => {
                          this.amigos = data;
                          // console.log(this.amigos);
                        }, (error) => {
                          console.log(error);
                        });

    this._as.getStatus().subscribe((status) => {
      this._us.getUserId(status.uid).valueChanges()
                          .subscribe((data: User) => {
                            this.user = data;
                            if (this.user.friends ) {
                              this.user.friends = Object.values(this.user.friends);
                              // console.log(this.user);
                            }

                          });
    });

  }

  ngOnInit() {
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  sendRequest() {
    const req = {
      timestamp: Date.now(),
      destinatario_email: this.friendEmail,
      remitente: this.user.uid,
      status: 'pending'
    };
    this._rs.createReq(req).then(() => {
      alert('solicitud enviada!');
    }).catch((error) => {
      alert('hubo un error!');
      console.log(error);
    });
  }

}
