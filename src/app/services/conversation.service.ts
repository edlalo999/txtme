import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ConversationService {

  constructor( private DB: AngularFireDatabase) { }

  createChat( conversation ) {
    return this.DB.object('conversations/' + conversation.uid +
    '/' + conversation.timestamp).set(conversation);
    // para referenciar cada uno de los nodos de la conversation se utiliza el timestmap
  }

  getChat( uid ) {
    return this.DB.list('conversations/' + uid);
  }

  editChat( conversation ) {
    return this.DB.object('conversations/' + conversation.uid +
    '/' + conversation.timestamp).set(conversation);
  }



}
