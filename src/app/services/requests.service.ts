import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(public db:AngularFireDatabase) {}

  createReq( request ) {
    // firebase no tolera puntos en los strings
    const cleanEmail = request.destinatario_email.replace('.', ',');
    return this.db.object('requests/' + cleanEmail + '/' + request.remitente).set(request);
  }

  setReqStatus( request, status ) {
    const cleanEmail = request.destinatario_email.replace('.', ',');
    return this.db.object('requests/' + cleanEmail + '/' + request.remitente + '/status').set(status);
  }

  getReqForEmail( email ) {
    const cleanEmail = email.replace('.', ',');
    return this.db.list('requests/' + cleanEmail);
  }
}
